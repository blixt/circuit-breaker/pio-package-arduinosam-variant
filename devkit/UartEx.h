#ifndef __UART_EX_H__
#define __UART_EX_H__

#include <cstddef>
#include <Uart.h>

#define UARTEX_PRINTF_BUF_SIZE  100
#define UARTEX_RX_BUF_SIZE      100

///< SLIP character types
#define SLIP_END                0xC0
#define SLIP_ESC                0xDB
#define SLIP_ESC_END            0xDC
#define SLIP_ESC_ESC            0xDD

#define SLIP_FRAME_TYPE_DBG     0x01

typedef void (* UartExInputCallback)(uint8_t type, uint8_t* data, uint32_t len);

class UartEx : public Uart {
public:

  UartEx(SERCOM* _s, uint8_t _pinRX, uint8_t _pinTX, SercomRXPad _padRX, SercomUartTXPad _padTX);
  UartEx(SERCOM* _s, uint8_t _pinRX, uint8_t _pinTX, SercomRXPad _padRX, SercomUartTXPad _padTX,
         uint8_t _pinRTS, uint8_t _pinCTS);
  void   printf(const char* format, ...);
  size_t dump(uint8_t type, uint8_t* data, uint32_t len);
  void   setSLIPEnable(bool en);
  void   setInputCallback(UartExInputCallback callback);
  void   poll();

protected:

  size_t _writeSLIP(uint8_t data);
  size_t _writeSLIP(uint8_t type, uint8_t* data, uint32_t len);
  void   _slip_input_byte(uint8_t data);
  bool _slip_enabled;
  uint8_t _printf_buf[UARTEX_PRINTF_BUF_SIZE];
  uint8_t _rx_buf[UARTEX_RX_BUF_SIZE];
  uint32_t _rx_data_len;
  uint8_t _slip_state;
  UartExInputCallback _input_callback;
};

#endif /* __UART_EX_H__ */
