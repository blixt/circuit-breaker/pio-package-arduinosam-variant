#ifndef __BCB_SERIAL_CONTROLLER_H__
#define __BCB_SERIAL_CONTROLLER_H__

#include <cstdint>

class BCBSerialController {
public:

  BCBSerialController();

  static void init();

protected:

  static void _loop();
  static void _onSerialData(uint8_t type, uint8_t* data, uint32_t len);
  static void _processsADCDumpCmd();
  static void _dumpADC();

  static bool _adcDumpEnabled;
};

#endif /* __BCB_SERIAL_CONTROLLER_H__ */
