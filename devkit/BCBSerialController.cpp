#include <BCBSerialController.h>
#include <Arduino.h>
#include <Scheduler.h>
#include <BCB.h>

#define DEBUG 0
#if DEBUG
# define PRINTF(...) Serial.printf(__VA_ARGS__)
#else
# define PRINTF(...)
#endif

enum DataFrameType {
  DATAFRAME_TYPE_NONE = 0,
  DATAFRAME_TYPE_DEBUG,
  DATAFRAME_TYPE_ADC,
  /* Commands */
  DATAFRAME_TYPE_COMMAND_ON = 20,
  DATAFRAME_TYPE_COMMAND_OFF,
  DATAFRAME_TYPE_COMMAND_OCP_OTP_RESET,
  DATAFRAME_TYPE_COMMAND_CALIBRATE,
  DATAFRAME_TYPE_COMMAND_GET_STATUS,
  DATAFRAME_TYPE_COMMAND_GET_CONFIG,
  DATAFRAME_TYPE_COMMAND_START_ADC_DUMP,
  DATAFRAME_TYPE_COMMAND_STOP_ADC_DUMP,
  /* Responses */
  DATAFRAME_TYPE_REPONSE_STATUS = 40,
  DATAFRAME_TYPE_REPONSE_CONFIG
};

typedef struct __attribute__((__packed__)) {
  uint32_t tick;
  uint16_t c1;
  uint16_t c2;
  uint16_t v;
} ADCFrame;

typedef struct __attribute__((__packed__)) {
  uint8_t on : 1;
  uint8_t pwr_good : 1;
  uint8_t ocp_triggered : 1;
  uint8_t otp_triggered : 1;
  uint8_t adc_dump_active : 1;
} StatusFrame;

typedef struct __attribute__((__packed__)) {
  uint16_t c1_zero;
  uint16_t c2_zero;
  uint16_t v_zero;
} ConfigFrame;

bool BCBSerialController::_adcDumpEnabled = false;

BCBSerialController::BCBSerialController() {
  _adcDumpEnabled = false;
}

void BCBSerialController::init() {
  Serial.setSLIPEnable(true);
  Serial.setInputCallback(BCBSerialController::_onSerialData);
  Scheduler.startLoop(BCBSerialController::_loop);
  PRINTF("SerialController initialized.\n");
}

void BCBSerialController::_loop() {
  Serial.poll();
  if (_adcDumpEnabled) {
    _dumpADC();
  }

  yield();
}

void BCBSerialController::_onSerialData(uint8_t type, uint8_t* data, uint32_t len) {
  switch (type) {
    case DATAFRAME_TYPE_COMMAND_ON:
      Serial.printf("CMD_ON\n");
      BCB::on();
      break;

    case DATAFRAME_TYPE_COMMAND_OFF:
      Serial.printf("CMD_OFF\n");
      BCB::off();
      break;

    case DATAFRAME_TYPE_COMMAND_OCP_OTP_RESET:
      Serial.printf("CMD_OCP_OTP_RESET\n");
      BCB::reset();
      break;

    case DATAFRAME_TYPE_COMMAND_CALIBRATE:
      Serial.printf("CMD_CALIBRATE\n");
      BCB::calibrate(true, true);
      break;

    case DATAFRAME_TYPE_COMMAND_GET_CONFIG:
      ConfigFrame config_frame;
      config_frame.c1_zero = BCB::getADCurrent1Zero();
      config_frame.c2_zero = BCB::getADCurrent2Zero();
      config_frame.v_zero = BCB::getADCVoltageZero();
      Serial.dump(DATAFRAME_TYPE_REPONSE_CONFIG, (uint8_t *)&config_frame,
                  (uint32_t)sizeof(ConfigFrame));
      break;

    case DATAFRAME_TYPE_COMMAND_GET_STATUS:
      StatusFrame status_frame;
      status_frame.on = BCB::isOn();
      status_frame.pwr_good = BCB::isPowerGood();
      status_frame.ocp_triggered     = BCB::isOCPTriggered();
      status_frame.otp_triggered     = BCB::isOTPTriggered();
      status_frame.adc_dump_active = _adcDumpEnabled;
      Serial.dump(DATAFRAME_TYPE_REPONSE_STATUS, (uint8_t *)&status_frame,
                  (uint32_t)sizeof(StatusFrame));
      break;

    case DATAFRAME_TYPE_COMMAND_START_ADC_DUMP:
      Serial.printf("CMD_START_ADC_DUMP\n");
      _adcDumpEnabled = true;
      break;

    case DATAFRAME_TYPE_COMMAND_STOP_ADC_DUMP:
      Serial.printf("CMD_STOP_ADC_DUMP\n");
      _adcDumpEnabled = false;
      break;

    default:
      Serial.printf("unhandled frame type %d, len %d\n", type, len);
      break;
  }
}

void BCBSerialController::_dumpADC() {
  ADCFrame adc_drame;

  adc_drame.tick = BCB::getADCTimestamp();
  adc_drame.c1  = (uint16_t)BCB::getADCCurrent1();
  adc_drame.c2  = (uint16_t)BCB::getADCCurrent2();
  adc_drame.v   = (uint16_t)BCB::getADCVoltage();

  Serial.dump(DATAFRAME_TYPE_ADC, (uint8_t *)&adc_drame, (uint32_t)sizeof(ADCFrame));
  Serial.flush();
}
