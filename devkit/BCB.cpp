#include <stdint.h>
#include <BCB.h>
#include <Print.h>
#include <Arduino.h>
#include <wiring_private.h>

#define DEBUG 0
#if DEBUG
# define PRINTF(...) Serial.printf(__VA_ARGS__)
#else
# define PRINTF(...)
#endif

#define BCB_PIN_UC_IN_SW_ON                 46
#define BCB_PIN_UC_IN_PWR_GOOD              47
#define BCB_PIN_UC_IN_OC_OT_SIG_HOLD        48
#define BCB_PIN_UC_IN_OC_SIG                49
#define BCB_PIN_UC_IN_OT_SIG                50
#define BCB_PIN_UC_IN_ZD_PULSE              51
#define BCB_PIN_UC_OUT_SW_ON                52
#define BCB_PIN_UC_OUT_RESET                53
#define BCB_PIN_ADC_VOLTAGE                 54
#define BCB_PIN_ADC_CURRENT_1               55
#define BCB_PIN_ADC_CURRENT_2               56
#define BCB_PIN_DAC_REF                     58
#define BCB_PIN_DAC_OUT                     A0
#define BCB_PIN_CM_OP_EN                    60
#define BCB_PIN_VM_OP_EN                    61
#define BCB_PIN_LED                         57

#define BCB_ADC_SAMPLING_CH_START           6
#define BCB_ADC_SAMPLING_NUM_CH             5

#define BCB_DAC_CURRENT2_START              200
#define BCB_DAC_CURRENT2_END                800

#define BCB_LINEARITY_ADJ_NUM_CM1           100
#define BCB_LINEARITY_ADJ_DEN_CM1           112
#define BCB_LINEARITY_ADJ_NUM_CM2           100
#define BCB_LINEARITY_ADJ_DEN_CM2           111
#define BCB_LINEARITY_ADJ_NUM_VM            100
#define BCB_LINEARITY_ADJ_DEN_VM            95

#define BCB_ZD_TIMESTAMP_DIFF_MIN           8000

#define NVM_MEMORY                          ((volatile uint16_t *)FLASH_ADDR)
#define NVM_CALIB_DATA_ADDR                 0x00001F00UL

#define NVM_CAL_POS_ADC_LINEARITY           27
#define NVM_CAL_SIZE_ADC_LINEARITY          8
#define NVM_CAL_POS_ADC_BIASCAL             35
#define NVM_CAL_SIZE_ADC_BIASCAL            3

#define NVM_READ_CAL(pos, size) (((*((uint32_t *)NVMCTRL_OTP4 + pos / 32)) >> (pos % 32)) & ((1 << size) - 1))

volatile uint32_t BCB::_adc_c1     = 0;
volatile uint32_t BCB::_adc_c2     = 0;
volatile uint32_t BCB::_adc_v      = 0;
volatile uint32_t BCB::_adc_ts     = 0;
volatile uint32_t BCB::_ts_zd_prev = 0;
uint32_t BCB::_adc_c1_zero        = 0;
uint32_t BCB::_adc_c2_zero        = 0;
uint32_t BCB::_adc_v_zero         = 0;
uint32_t BCB::_dac_c2_zero        = 0;
uint32_t BCB::_flash_page_size    = 0;
uint32_t BCB::_flash_n_pages      = 0;
uint32_t BCB::_flash_max_size     = 0;
volatile bool BCB::_otp_triggered = false;
volatile bool BCB::_ocp_triggered = false;
BCBCallbackON BCB::_cb_on         = NULL;
BCBCallbackZD BCB::_cb_zd         = NULL;
BCBCallbackOC BCB::_cb_oc         = NULL;
BCBCallbackOT BCB::_cb_ot         = NULL;

typedef struct __attribute__((__packed__)) {
  uint32_t adc_v_zero;
} ADCCalibrationData;

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief ADC interrupt handler helper fuction
 *
 */
inline void _adc_irq() {
  /*
   * BCB_PIN_ADC_CURRENT_1 - ADC_CH06
   * BCB_PIN_ADC_CURRENT_2 - ADC_CH07
   * BCB_PIN_ADC_VOLTAGE   - ADC_CH10
   */
  uint32_t adc = ADC->RESULT.reg;
  /* We do not need to synchronize reading the COUNT register since we've done a continious
   * read/update request in _setupADCTimestampTimer().
   */
  uint32_t t = TC4->COUNT32.COUNT.reg;

  /* INPUTOFFSET is incremented by one after every conversion */
  switch (ADC->INPUTCTRL.bit.INPUTOFFSET) {
    case 1:
      BCB::_adc_c1 = adc;
      break;

    case 2:
      BCB::_adc_c2 = adc;
      break;

    case 0:
      BCB::_adc_v  = adc;
      BCB::_adc_ts = t;
      break;

    default:
      break;
  }
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Implementation ADC interrupt handler.
 * We can't make the interrupt handler a friend function.
 * So we use a helper function to change internals of BCB class.
 */
void ADC_Handler() {
  if (ADC->INTFLAG.bit.RESRDY) {
    _adc_irq();
    ADC->INTFLAG.bit.RESRDY = 1;
  }
  if (ADC->INTFLAG.bit.OVERRUN) {
    ADC->INTFLAG.bit.OVERRUN = 1;
  }
  if (ADC->INTFLAG.bit.WINMON) {
    ADC->INTFLAG.bit.WINMON = 1;
  }
  if (ADC->INTFLAG.bit.SYNCRDY) {
    ADC->INTFLAG.bit.SYNCRDY = 1;
  }
}

BCB::BCB() {}

/*------------------------------------------------------------------------------------------------*/
void BCB::_onZDPulse() {
  /* We use the timestamping timer to check the time duration between two zero-detect interrupts.
   * If the duration is less than 4 milliseconds, we ignore the interrupt.
   *
   * Read the value of the counter first before doing any processing.
   * Note that we do not need to synchronize reading the COUNT register since we've done a
   * continious read/update request in _setupADCTimestampTimer().
   *
   * TC4 and TC5 is used as a pair in 32-bit mode with 2 MHz clock.
   * TC4 is the master and TC5 is the slave.
   */
  volatile uint32_t ts_now  = TC4->COUNT32.COUNT.reg;
  volatile uint32_t ts_diff = 0;

  if (_ts_zd_prev > ts_now) {
    /* interger overflow happended */
    ts_diff = (UINT32_MAX - _ts_zd_prev) + ts_now;
  } else {
    ts_diff = ts_now - _ts_zd_prev;
  }

  if (ts_diff > BCB_ZD_TIMESTAMP_DIFF_MIN) {
    _ts_zd_prev = ts_now;
    /* Previous interrupt happened 4 ms ago. */
    if (_cb_zd) {
      _cb_zd();
    }
  } else {
    /* Ignore false positive interruptss */
  }
}

/*------------------------------------------------------------------------------------------------*/
void BCB::_onOCP() {
  _ocp_triggered = true;
  if (_cb_oc) {
    _cb_oc();
  }
}

/*------------------------------------------------------------------------------------------------*/
void BCB::_onOTP() {
  _otp_triggered = true;
  if (_cb_ot) {
    _cb_ot();
  }
}

/*------------------------------------------------------------------------------------------------*/
void BCB::_onON() {
  if (_cb_on) {
    _cb_on();
  }
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Initialize the ADC.
 *
 */
void BCB::_adcInit() {
  /* Core (GEN_GCLK0) is running at 32MHz (see VARIANT_MCK in variant.h of the platform variant).
   * The Generic Clock and synchronous APBC clock (user interface clock) for the ADC
   * are already configured in init() in wiring.c
   */
  /* Wait for synchronization from any previous operations */
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* Disable ADC before doing any changes */
  ADC->CTRLA.bit.ENABLE = 0;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* Load ADC calibration from the NVM and set */
  uint32_t biascal   = NVM_READ_CAL(NVM_CAL_POS_ADC_BIASCAL, NVM_CAL_SIZE_ADC_BIASCAL);
  uint32_t linearity = NVM_READ_CAL(NVM_CAL_POS_ADC_LINEARITY, NVM_CAL_SIZE_ADC_LINEARITY);
  ADC->CALIB.reg = ADC_CALIB_BIAS_CAL(biascal) | ADC_CALIB_LINEARITY_CAL(linearity);
  /* Divide Clock by 32 and 12 bits resolution 
   * It takes (12 / 2 + 1) = 7 cycles to do a single sample since no extra samples taken.
   * So sample rate =  GEN_GCLK0 / (32 * 7) ~ 142857 samples/second 
   */
  ADC->CTRLB.reg = ADC_CTRLB_PRESCALER_DIV64 | ADC_CTRLB_RESSEL_12BIT;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* Average control 1 sample, no right-shift */
  ADC->AVGCTRL.reg = ADC_AVGCTRL_ADJRES(0) | ADC_AVGCTRL_SAMPLENUM_1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* Sampling time, no extra sampling half clock-cycles
   * Sampling time = (Sample Len + 1)(CLK_ADC / 2)
   */
  ADC->SAMPCTRL.reg = ADC_SAMPCTRL_SAMPLEN(0);
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* We use internal voltage reference VDDANA/2 (1.25 V)
   * So, we halve the gain to fit the input to the range 0 - 1.25V (VDDNA/2)
   * Always use GND for the negative input
   */
  ADC->REFCTRL.reg = ADC_REFCTRL_REFSEL_INTVCC1 | ADC_REFCTRL_REFCOMP;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  ADC->INPUTCTRL.reg = ADC_INPUTCTRL_GAIN_DIV2 | ADC_INPUTCTRL_MUXNEG_GND;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* The first conversion after the reference is changed must not be used.
   * So  start a dummy reading
   */
  ADC->CTRLA.bit.ENABLE = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  ADC->SWTRIG.bit.START = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  while (ADC->INTFLAG.bit.RESRDY == 0);
  ADC->RESULT.reg; // force register read
  /* Disable ADC */
  ADC->CTRLA.bit.ENABLE = 0;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Initialize the DAC.
 *
 */
void BCB::_dacInit() {
  /* Core (GEN_GCLK0) is running at 32MHz (see VARIANT_MCK in variant.h of the platform variant).
   * The Generic Clock and synchronous APBC clock (user interface clock) for the DAC
   * are already configured in init() in wiring.c
   */
  /* Wait for synchronization from any previous operations */
  while (DAC->STATUS.bit.SYNCBUSY == 1);
  /* Disable ADC before doing any changes */
  DAC->CTRLA.bit.ENABLE = 0;
  while (DAC->STATUS.bit.SYNCBUSY == 1);
  /* Use VDDANA (2.5V) as the reference and Enable output.*/
  DAC->CTRLB.reg = DAC_CTRLB_REFSEL_AVCC | DAC_CTRLB_EOEN;
  while (DAC->STATUS.bit.SYNCBUSY == 1);
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Read the ADC in blocking mode.
 *
 * @param pin Index of the analog input pin in the pin description array g_APinDescription
 * @param n_avg_samples_pw Number of samples to be taken for averaging.
 *                         This should be the power of a number that is a power of two.
 *                         Eg. 8 if 256 samples to be taken.
 * @return The ADC reading as a 32-bit unsigned integer.
 */
uint32_t BCB::_readADC(uint8_t pin, uint8_t n_avg_samples_pw) {
  /* Get the corresponding ADC channel */
  uint8_t adc_ch = g_APinDescription[pin].ulADCChannelNumber;

  /* Disable the ADC before doing changes */
  ADC->CTRLA.bit.ENABLE = 0;
  while (ADC->STATUS.bit.SYNCBUSY == 1);

  ADC_CTRLB_Type ctrlb;
  ctrlb.reg = ADC->CTRLB.reg;
  ctrlb.bit.FREERUN = 0;
  if (n_avg_samples_pw == 0) {
    ctrlb.bit.RESSEL = ADC_CTRLB_RESSEL_12BIT_Val;
  } else {
    ctrlb.bit.RESSEL = ADC_CTRLB_RESSEL_16BIT_Val;
  }
  ADC->CTRLB.reg = ctrlb.reg;
  while (ADC->STATUS.bit.SYNCBUSY == 1);

  ADC_INPUTCTRL_Type input_ctrl;
  input_ctrl.reg = ADC->INPUTCTRL.reg;
  input_ctrl.bit.MUXPOS = adc_ch;
  input_ctrl.bit.INPUTOFFSET = 0;
  input_ctrl.bit.INPUTSCAN   = 0;
  ADC->INPUTCTRL.reg = input_ctrl.reg;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* Average control, */
  ADC_AVGCTRL_Type avgctrl;
  avgctrl.reg = ADC->AVGCTRL.reg;
  avgctrl.bit.ADJRES    = (n_avg_samples_pw < 4) ? n_avg_samples_pw : 4;
  avgctrl.bit.SAMPLENUM = n_avg_samples_pw;
  ADC->AVGCTRL.reg = avgctrl.reg;
  while (ADC->STATUS.bit.SYNCBUSY == 1);

  ADC->INTFLAG.bit.RESRDY = 1;
  ADC->CTRLA.bit.ENABLE   = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);

  ADC->SWTRIG.bit.START = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  while (ADC->INTFLAG.bit.RESRDY == 0);

  uint32_t adc = ADC->RESULT.reg;

  ADC->CTRLA.bit.ENABLE = 0;
  while (ADC->STATUS.bit.SYNCBUSY == 1);

  return adc;
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Write to the DAC.
 *
 * @param val 10-bit value to be output.
 */
void BCB::_writeDAC(uint32_t val) {
  while (DAC->STATUS.bit.SYNCBUSY == 1);
  DAC->DATA.reg = val & 0x3FF; // 10-bit
  while (DAC->STATUS.bit.SYNCBUSY == 1);
  DAC->CTRLA.bit.ENABLE = 1;
  while (DAC->STATUS.bit.SYNCBUSY == 1);
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Enable/Disable current measurement Opamps.
 *
 * @param en If true, the opamps will be enabled.
 */
void BCB::_enCMOPAmp(bool en) {
  if (en) {
    digitalWrite(BCB_PIN_CM_OP_EN, 1);
  } else {
    digitalWrite(BCB_PIN_CM_OP_EN, 0);
  }
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Enable/Disable voltage measurement Opamp.
 *
 * @param en If true, the opamp will be enabled.
 */
void BCB::_enVMOPAmp(bool en) {
  if (en) {
    digitalWrite(BCB_PIN_VM_OP_EN, 1);
  } else {
    digitalWrite(BCB_PIN_VM_OP_EN, 0);
  }
}

/*------------------------------------------------------------------------------------------------*/
void BCB::init() {
  _adc_c1 = 0;
  _adc_c1_zero = 0;
  _adc_c2 = 0;
  _adc_c2_zero = 0;
  _adc_v = 0;
  _adc_v_zero    = 0;
  _otp_triggered = false;
  _ocp_triggered = false;

  pinMode(BCB_PIN_UC_IN_SW_ON, INPUT);
  pinMode(BCB_PIN_UC_IN_PWR_GOOD, INPUT);
  pinMode(BCB_PIN_UC_IN_OC_OT_SIG_HOLD, INPUT);
  pinMode(BCB_PIN_UC_IN_ZD_PULSE, INPUT);
  pinMode(BCB_PIN_UC_IN_OC_SIG, INPUT);
  pinMode(BCB_PIN_UC_IN_OT_SIG, INPUT);
  pinMode(BCB_PIN_UC_IN_ZD_PULSE, INPUT);
  pinMode(BCB_PIN_LED, OUTPUT);
  pinMode(BCB_PIN_UC_OUT_SW_ON, OUTPUT);
  pinMode(BCB_PIN_UC_OUT_RESET, OUTPUT);
  pinMode(BCB_PIN_CM_OP_EN, OUTPUT);
  pinMode(BCB_PIN_VM_OP_EN, OUTPUT);
  pinMode(BCB_PIN_ADC_VOLTAGE, INPUT);
  pinMode(BCB_PIN_ADC_CURRENT_1, INPUT);
  pinMode(BCB_PIN_ADC_CURRENT_2, INPUT);
  pinPeripheral(BCB_PIN_ADC_VOLTAGE, PIO_ANALOG);
  pinPeripheral(BCB_PIN_ADC_CURRENT_1, PIO_ANALOG);
  pinPeripheral(BCB_PIN_ADC_CURRENT_2, PIO_ANALOG);

  _adcInit();
  _dacInit();

  reset();
  off();
  ledOff();

  _flashGetInfo();
  calibrate(false, false);
  _setupTimestampTimer();
  _startSampling();

  attachInterrupt(digitalPinToInterrupt(BCB_PIN_UC_IN_SW_ON), BCB::_onON, RISING);
  attachInterrupt(digitalPinToInterrupt(BCB_PIN_UC_IN_ZD_PULSE), BCB::_onZDPulse, RISING);
  attachInterrupt(digitalPinToInterrupt(BCB_PIN_UC_IN_OT_SIG), BCB::_onOTP, RISING);
  Serial.printf("* Blixt Circuit Breaker *\n");
  Serial.printf("CHIP ID: %" PRIx32 ":%" PRIx32 ":%" PRIx32 ":%" PRIx32 " \n",
                getChipIDP1(), getChipIDP2(), getChipIDP3(), getChipIDP4());
}

/*------------------------------------------------------------------------------------------------*/
void BCB::on() {
  reset();
  attachInterrupt(digitalPinToInterrupt(BCB_PIN_UC_IN_OC_SIG), BCB::_onOCP, RISING);
  digitalWrite(BCB_PIN_UC_OUT_SW_ON, 1);
}

/*------------------------------------------------------------------------------------------------*/
void BCB::off() {
  /*
   * First, disable the overcurrent interrupt since it may also be triggered
   * during the turning off of the power transistors.
   * Could this be due to high current when discharging the gate capacitors?
   */
  detachInterrupt(digitalPinToInterrupt(BCB_PIN_UC_IN_OC_SIG));
  digitalWrite(BCB_PIN_UC_OUT_SW_ON, 0);
  reset();
}

/*------------------------------------------------------------------------------------------------*/
void BCB::ledOn() {
  digitalWrite(BCB_PIN_LED, HIGH);
}

/*------------------------------------------------------------------------------------------------*/
void BCB::ledOff() {
  digitalWrite(BCB_PIN_LED, LOW);
}

/*------------------------------------------------------------------------------------------------*/
void BCB::reset() {
  /* Generate reset pulse */
  digitalWrite(BCB_PIN_UC_OUT_RESET, 0);
  digitalWrite(BCB_PIN_UC_OUT_RESET, 1);
  digitalWrite(BCB_PIN_UC_OUT_RESET, 0);
  _ocp_triggered = false;
  _otp_triggered = false;
}

/*------------------------------------------------------------------------------------------------*/

bool BCB::isOn() {
  return digitalRead(BCB_PIN_UC_IN_SW_ON) ? true : false;
}

bool BCB::isPowerGood() {
  return digitalRead(BCB_PIN_UC_IN_PWR_GOOD) ? true : false;
}

bool BCB::isOCPTriggered() {
  /*
   * BCB_PIN_UC_IN_OC_OT_SIG_HOLD line become high for both over current and over temperature
   * events.
   * So, we need to check if the over temperature event has happened.
   * Note that BCB_PIN_UC_IN_OC_OT_SIG_HOLD is inverted.
   */
  return _ocp_triggered;
}

bool BCB::isOTPTriggered() {
  /*
   * We directly read the input pin since temperature changes are very slow.
   */
  return _otp_triggered;
}

/*------------------------------------------------------------------------------------------------*/
int32_t BCB::getCurrent() {
  /**
   * The current measurement path has two amplification stages such that each has x16 gain.
   * The voltage drop across two 2 mOhm resistors are used for current measurement.
   * ADC is 12-bit and reference voltage is 2.5 V
   * So current I = ADC_DIFF * (2.5/2^12) / (0.002 * 16 * 16)
   * I (in mA) = ADC_DIFF * 5 * 10^6 / 2^22
   */
  uint64_t val = 0;
  int8_t sign  = 0;
  volatile uint32_t adc1_tmp = _adc_c1;
  volatile uint32_t adc2_tmp = _adc_c2;

  if (adc2_tmp < 4000 && adc2_tmp > 100) {
    /* Use low range for accuracy */
    if (adc2_tmp < _adc_c2_zero) {
      sign = -1;
      val  = ((uint64_t)(_adc_c2_zero - adc2_tmp) * 5 * 1000000) >> 22;
    } else {
      sign = 1;
      val  = ((uint64_t)(adc2_tmp - _adc_c2_zero) * 5 * 1000000) >> 22;
    }
    val = (val * BCB_LINEARITY_ADJ_NUM_CM2) / BCB_LINEARITY_ADJ_DEN_CM2;
  } else {
    /* low range is about to saturate, So we use the high range */
    if (adc1_tmp < _adc_c1_zero) {
      sign = -1;
      val  = ((uint64_t)(_adc_c1_zero - adc1_tmp) * 5 * 1000000) >> 18;
    } else {
      sign = 1;
      val  = ((uint64_t)(adc1_tmp - _adc_c1_zero) * 5 * 1000000) >> 18;
    }
    val = (val * BCB_LINEARITY_ADJ_NUM_CM1) / BCB_LINEARITY_ADJ_DEN_CM1;
  }

  return (int32_t)val * sign;
}

/*------------------------------------------------------------------------------------------------*/
int32_t BCB::getVoltage() {
  /*
   * VM_OP1_OUT - Output voltage of the opamp
   * V_MP - Input voltage to the opamp circuit
   * G - Gain of the opamp circuit
   * V_REF_VM - Input bias voltage of the opamp circuit
   * V_REF_HIGH_VM - High voltage input to be measured
   *
   * VM_OP1_OUT = -G*V_MP + (1 + G)*V_REF_VM
   * V_REF_HIGH_VM = (1 + 2Meg/5k)*V_MP
   *
   * We cancel out the "(1 + G)*V_REF_VM" by substracting VM_OP1_OUT when V_MP = 0.
   * The gain G = 1
   * Therefore,
   * V_REF_HIGH_VM = (1 + 2Meg/5k)*(-VM_OP1_OUT)
   * VM_OP1_OUT = ADC_DIFF * 2.5 / 2^12
   * V_REF_HIGH_VM = (1 + 2Meg/5k)*(-ADC_DIFF * 5 * 10^3 / 2^13)
   */
  uint64_t val = 0;
  int8_t sign  = 0;
  volatile uint32_t adc_v_tmp = _adc_v;

  if (adc_v_tmp < _adc_v_zero) {
    sign = -1;
    val  = (401 * (_adc_v_zero - adc_v_tmp) * 5 * 1000) >> 13;
  } else {
    sign = 1;
    val  = (401 * (adc_v_tmp - _adc_v_zero) * 5 * 1000) >> 13;
  }
  return (int32_t)(val * BCB_LINEARITY_ADJ_NUM_VM / BCB_LINEARITY_ADJ_DEN_VM) * sign;
}

/*------------------------------------------------------------------------------------------------*/
void BCB::setCallbackON(BCBCallbackON callback) {
  _cb_on = callback;
}

/*------------------------------------------------------------------------------------------------*/
void BCB::setCallbackZD(BCBCallbackZD callback) {
  _cb_zd = callback;
}

/*------------------------------------------------------------------------------------------------*/
void BCB::setCallbackOC(BCBCallbackOC callback) {
  _cb_oc = callback;
}

/*------------------------------------------------------------------------------------------------*/
void BCB::setCallbackOT(BCBCallbackOT callback) {
  _cb_ot = callback;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getADCCurrent1() {
  return _adc_c1;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getADCCurrent2() {
  return _adc_c2;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getADCVoltage() {
  return _adc_v;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getADCTimestamp() {
  return _adc_ts;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getADCurrent1Zero() {
  return _adc_c1_zero;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getADCurrent2Zero() {
  return _adc_c2_zero;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getADCVoltageZero() {
  return _adc_v_zero;
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getChipIDP1() {
  return (uint32_t)(*(volatile uint32_t *)0x0080A00C);
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getChipIDP2() {
  return (uint32_t)(*(volatile uint32_t *)0x0080A040);
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getChipIDP3() {
  return (uint32_t)(*(volatile uint32_t *)0x0080A044);
}

/*------------------------------------------------------------------------------------------------*/
uint32_t BCB::getChipIDP4() {
  return (uint32_t)(*(volatile uint32_t *)0x0080A048);
}

/*------------------------------------------------------------------------------------------------*/
void BCB::_setupTimestampTimer() {
  /* We use two TCs in master-slave mode to make a 32-bit TC with 2 MHz clock.
   * NOTE:
   * The datasheet mentions that TC3 and TC4 is paired to make 32-bit counter.
   * However, it turns out TC4 and TC5 is the pair and TC4 is the master and TC5 is the slave.
   *
   * Core (GEN_GCLK0) is runing at 32 MHz (see VARIANT_MCK in variant.h of the platform variant).
   * Synchronous APBC clock (peripheral interface clock) for the TC4 and TC5 is already enabled in
   * wiring.c
   */

  /* Enable Generic Clock for the TC4 and TC5 */
  GCLK->CLKCTRL.reg = (uint16_t)(GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID(GCM_TC4_TC5));
  while (GCLK->STATUS.bit.SYNCBUSY == 1);
  /* Wait for any previous operations */
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* TC4 must be disabled before modifying the its registers */
  TC4->COUNT32.CTRLA.bit.ENABLE = 0;
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Set the timer clock to 2 MHz and 32-bit mode */
  uint16_t ctrla = TC_CTRLA_PRESCSYNC_PRESC | TC_CTRLA_PRESCALER_DIV16 | TC_CTRLA_MODE_COUNT32;
  TC4->COUNT32.CTRLA.reg = ctrla;
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Count upwards (clearing all bits makes the counter count up). */
  TC4->COUNT32.CTRLBCLR.reg = 0xFF;
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Set the counter to zero */
  TC4->COUNT32.COUNT.reg = 0;
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Request continous read/update to the count register to
     avoid read synchronization. */
  TC4->COUNT32.READREQ.reg = TC_READREQ_RREQ | TC_READREQ_RCONT | TC_READREQ_ADDR(0x10);
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Disable all interrupts */
  TC4->COUNT32.INTENCLR.reg = TC_INTENCLR_MASK;
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Clear all interrupt flags */
  TC4->COUNT32.INTFLAG.reg = TC_INTFLAG_MASK;
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Enable the timer */
  TC4->COUNT32.CTRLA.bit.ENABLE = 1;
  while (TC4->COUNT32.STATUS.bit.SYNCBUSY == 1);
  /* Set the previous zero-detect timestamp */
  _ts_zd_prev = TC4->COUNT32.COUNT.reg;
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Start ADC sampling
 *
 */
void BCB::_startSampling() {
  _enVMOPAmp(true);
  _enCMOPAmp(true);
  /* Disable the ADC prior to modify */
  ADC->CTRLA.bit.ENABLE = 0;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  ADC_INPUTCTRL_Type input_ctrl;
  input_ctrl.reg = ADC->INPUTCTRL.reg;
  input_ctrl.bit.MUXPOS = BCB_ADC_SAMPLING_CH_START;
  input_ctrl.bit.INPUTOFFSET = 0;
  input_ctrl.bit.INPUTSCAN   = BCB_ADC_SAMPLING_NUM_CH - 1;
  ADC->INPUTCTRL.reg = input_ctrl.reg;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* Average control 1 sample, no right-shift */
  ADC->AVGCTRL.reg = ADC_AVGCTRL_ADJRES(0) | ADC_AVGCTRL_SAMPLENUM_1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  /* Eanble free running mode, clear interrupts and enable ADC */
  ADC->CTRLB.bit.FREERUN = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  ADC->INTFLAG.bit.RESRDY  = 1;
  ADC->INTENSET.bit.RESRDY = 1;
  ADC->CTRLA.bit.ENABLE    = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  NVIC_EnableIRQ(ADC_IRQn);
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Stop ADC sampling
 *
 */
void BCB::_stopSampling() {
  // Disable opamps
  _enVMOPAmp(false);
  _enCMOPAmp(false);
  /* Disable ADC, result ready interrupt and free running mode */
  ADC->CTRLA.bit.ENABLE = 0;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  ADC->INTENSET.bit.RESRDY = 0;
  ADC->CTRLB.bit.FREERUN   = 0;
  while (ADC->STATUS.bit.SYNCBUSY == 1);
  NVIC_DisableIRQ(ADC_IRQn);
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Retrieve flash parameters.
 *
 */
void BCB::_flashGetInfo() {
  uint32_t page_sizes[] = { 8, 16, 32, 64, 128, 256, 512, 1024 };

  _flash_page_size = page_sizes[NVMCTRL->PARAM.bit.PSZ];
  _flash_n_pages   = NVMCTRL->PARAM.bit.NVMP;
  _flash_max_size  = _flash_page_size * _flash_n_pages;
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Erase a row of the flash memory.
 *        The flash memory is erased in ROWS, that is in block of 4 pages.
 *
 * @param addr Address of a row to be erased.
 */
bool BCB::_flashEraseRow(uint32_t addr) {
  /* Check if the row address is not valid */
  if (addr > (FLASH_ADDR + _flash_max_size)) {
    return false;
  }
  /* Check if the address to erase is not aligned to the start of a row */
  if (addr & ((_flash_page_size * NVMCTRL_ROW_PAGES) - 1)) {
    return false;
  }
  /* Wait for any previous operations */
  while (NVMCTRL->INTFLAG.bit.READY == 0);
  /* Clear error flags */
  NVMCTRL->STATUS.reg = NVMCTRL_STATUS_MASK;
  /* The address is specified in terms of 16-bit words. */
  NVMCTRL->ADDR.reg = addr / 2;
  /* Issue erase command */
  NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_ER;
  while (NVMCTRL->INTFLAG.bit.READY == 0);
  /* Check for errors */
  if (NVMCTRL->STATUS.reg & (NVMCTRL_STATUS_PROGE | NVMCTRL_STATUS_LOCKE | NVMCTRL_STATUS_NVME)) {
    return false;
  } else {
    return true;
  }
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Write data to a page of the flash memory.
 *
 * @param addr Address of a flash page.
 * @param buf A pointer to the data to be written.
 * @param len  Length of the data to be written. This should be less than or eaqual to the
 *             page size.
 */
bool BCB::_flashWritePage(uint32_t addr, uint8_t* buf, uint32_t len) {
  /* Check if the page address is not valid */
  if (addr > (FLASH_ADDR + _flash_max_size)) {
    return false;
  }
  /* Check if the address is not aligned to the start of a page */
  if (addr & (_flash_page_size - 1)) {
    return false;
  }
  /* Check if the length is longer than the size of a page */
  if (len > _flash_page_size) {
    return false;
  }
  /* Wait for any previous operations */
  while (NVMCTRL->INTFLAG.bit.READY == 0);
  /* Clear error flags */
  NVMCTRL->STATUS.reg = NVMCTRL_STATUS_MASK;
  /* Erase the page buffer before buffering new data */
  NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMD_PBC | NVMCTRL_CTRLA_CMDEX_KEY;
  while (NVMCTRL->INTFLAG.bit.READY == 0);
  /* We will do a manual page write. */
  NVMCTRL->CTRLB.bit.MANW = 1;
  /* The address is specified in terms of 16-bit words. */
  uint32_t nvm_addr = addr / 2;
  /* NVM _must_ be accessed as a series of 16-bit(or 32-bit) words.
   * We manually copy to ensure alignment
   */
  for (uint16_t i = 0; i < len; i += 2) {
    uint16_t data;
    /* Copy first byte of the 16-bit chunk to the temporary buffer */
    data = buf[i];
    /* If we are not at the end of a write request with an odd byte count,
     * store the next byte of data as well */
    if (i < (len - 1)) {
      data |= (buf[i + 1] << 8);
    }
    /* Store next 16-bit chunk to the NVM memory space */
    NVM_MEMORY[nvm_addr++] = data;
  }
  /* Execute page write command */
  NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_WP;
  while (NVMCTRL->INTFLAG.bit.READY == 0);
  /* Check for errors */
  if (NVMCTRL->STATUS.reg & (NVMCTRL_STATUS_PROGE | NVMCTRL_STATUS_LOCKE | NVMCTRL_STATUS_NVME)) {
    return false;
  } else {
    return true;
  }
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Read data from a page of the flash memory.
 *
 * @param addr Address of a flash page.
 * @param buf A pointer to a buffer that the data should be stored.
 * @param len  Length of the data to be read. This should be less than or eaqual to the
 *             page size.
 */
bool BCB::_flashReadPage(uint32_t addr, uint8_t* buf, uint32_t len) {
  /* Check if the page address is not valid */
  if (addr > (FLASH_ADDR + _flash_max_size)) {
    return false;
  }
  /* Check if the address is not aligned to the start of a page */
  if (addr & (_flash_page_size - 1)) {
    return false;
  }
  /* Check if the length is longer than the size of a page */
  if (len > _flash_page_size) {
    return false;
  }
  /* Wait for any previous operations */
  while (NVMCTRL->INTFLAG.bit.READY == 0);
  /* Clear error flags */
  NVMCTRL->STATUS.reg = NVMCTRL_STATUS_MASK;
  /* The address is specified in terms of 16-bit words. */
  uint32_t nvm_addr = addr / 2;
  /* NVM _must_ be accessed as a series of 16-bit(or 32-bit) words.
   * We manually copy to ensure alignment
   */
  for (uint16_t i = 0; i < len; i += 2) {
    /* Fetch next 16-bit chunk from the NVM memory space */
    uint16_t data = NVM_MEMORY[nvm_addr++];
    /* Copy first byte of the 16-bit chunk to the destination buffer */
    buf[i] = (data & 0xFF);
    /* If we are not at the end of a read request with an odd byte count,
     * store the next byte of data as well */
    if (i < (len - 1)) {
      buf[i + 1] = (data >> 8);
    }
  }
  /* Check for errors */
  if (NVMCTRL->STATUS.reg & (NVMCTRL_STATUS_PROGE | NVMCTRL_STATUS_LOCKE | NVMCTRL_STATUS_NVME)) {
    return false;
  } else {
    return true;
  }
}

/*------------------------------------------------------------------------------------------------*/
/**
 * @brief Calibrate ADC and store the calibration data on the NVM flash.
 *
 * @param vm Set true if voltage measurement ADC needs to be calibrated.
 *           NOTE: Voltage measurement input should not be connected during the calibration.
 * @param save Set true if calibration data need to be stored on the NVM flash.
 */
void BCB::calibrate(bool vm, bool save) {
  uint32_t dac     = 0;
  uint32_t c2_prev = 0;
  uint32_t c2 = 0;
  uint32_t ext_ref = 0;
  bool status __attribute__((__unused__)) = false;
  ADCCalibrationData data;

  status = _flashReadPage(NVM_CALIB_DATA_ADDR, (uint8_t *)&data, sizeof(ADCCalibrationData));
  /* We only load the idle ADC value related to the voltage measurement.
   * Others are calculated on the fly.
   */
  _adc_v_zero = data.adc_v_zero;

  /* Stop sampling before calibration */
  _stopSampling();
  /* Enable both current and voltage measurement Opamps */
  _enCMOPAmp(true);
  _enVMOPAmp(true);

  /* Read the external reference voltage. VDDA/2 */
  ext_ref = _readADC(58, 8);
  /* Adjust low range current measurement opamp to the center based on the external reference */
  for (dac = BCB_DAC_CURRENT2_START; dac < BCB_DAC_CURRENT2_END; dac++) {
    _writeDAC(dac);
    c2 = _readADC(BCB_PIN_ADC_CURRENT_2, 8);
    if (c2 < ext_ref) {
      if ((ext_ref - c2) > (c2_prev - ext_ref)) {
        /* Use the previous value since it provides the minimum diff */
        dac--;
      }
      break;
    }
    c2_prev = c2;
  }

  _dac_c2_zero = dac;
  _writeDAC(_dac_c2_zero);
  _adc_c1_zero = _readADC(BCB_PIN_ADC_CURRENT_1, 8);
  _adc_c2_zero = _readADC(BCB_PIN_ADC_CURRENT_2, 8);
  if (vm) {
    /* Voltage measurement input should not be connected during the calibration.*/
    _adc_v_zero = _readADC(BCB_PIN_ADC_VOLTAGE, 8);
  }

  PRINTF("[CAL] ext_ref %" PRIu32 ", adc_c1 %" PRIu32 ", adc_c2 %" PRIu32 ", adc_v %" PRIu32 ", dac_c2 %" PRIu32 "]\n",
         ext_ref,
         _adc_c1_zero,
         _adc_c2_zero,
         _adc_v_zero,
         _dac_c2_zero);

  if (save) {
    data.adc_v_zero = _adc_v_zero;
    /* Erase the entire NVM row before writing */
    status = _flashEraseRow(NVM_CALIB_DATA_ADDR);
    PRINTF("[CAL] NVM_ERASE: %s\n", status ? "success" : "failed");
    /* Write calibration data to the NVM */
    status = _flashWritePage(NVM_CALIB_DATA_ADDR, (uint8_t *)&data, sizeof(ADCCalibrationData));
    PRINTF("[CAL] NVM_WRITE: %s\n", status ? "success" : "failed");
  }
  Serial.flush();

  /* Disable all opamps */
  _enVMOPAmp(false);
  _enCMOPAmp(false);
  /* Restart sampling after the calibration */
  _startSampling();
}
