#include <UartEx.h>

#include <stdarg.h>
#include <string.h>

#define NO_RTS_PIN      255
#define NO_CTS_PIN      255

enum {
  SLIP_STATE_RUBBISH = 0,
  SLIP_STATE_OK,
  SLIP_STATE_ESC,
  SLIP_STATE_PROCESSING
};

UartEx::UartEx(SERCOM* _s, uint8_t _pinRX, uint8_t _pinTX, SercomRXPad _padRX,
               SercomUartTXPad _padTX) :
  Uart(_s, _pinRX, _pinTX, _padRX, _padTX, NO_RTS_PIN, NO_CTS_PIN) {
  _slip_enabled   = false;
  _input_callback = NULL;
  _slip_state     = SLIP_STATE_RUBBISH;
}

UartEx::UartEx(SERCOM* _s, uint8_t _pinRX, uint8_t _pinTX, SercomRXPad _padRX,
               SercomUartTXPad _padTX, uint8_t _pinRTS, uint8_t _pinCTS) :
  Uart(_s, _pinRX, _pinTX, _padRX, _padTX, _pinRTS, _pinCTS) {
  _slip_enabled = false;
}

void UartEx::printf(const char* format, ...) {
  uint32_t len = 0;
  va_list ap;

  va_start(ap, format);
  len = vsnprintf((char *)_printf_buf, UARTEX_PRINTF_BUF_SIZE, format, ap);
  va_end(ap);

  if (_slip_enabled) {
    _writeSLIP(SLIP_FRAME_TYPE_DBG, _printf_buf, len);
  } else {
    write(_printf_buf, len);
  }
}

void UartEx::setSLIPEnable(bool en) {
  _slip_enabled = en;
  _rx_data_len  = 0;
  _slip_state   = SLIP_STATE_RUBBISH;
}

size_t UartEx::dump(uint8_t type, uint8_t* data, uint32_t len) {
  if (_slip_enabled) {
    return _writeSLIP(type, data, len);
  } else {
    return write(data, len);
  }

  return 0;
}

size_t UartEx::_writeSLIP(uint8_t data) {
  switch (data) {
    case SLIP_END:
      write(SLIP_ESC);
      return write(SLIP_ESC_END) + 1;
      break;

    case SLIP_ESC:
      write(SLIP_ESC);
      return write(SLIP_ESC_ESC) + 1;
      break;

    default:
      return write(data);
      break;
  }

  return 0;
}

size_t UartEx::_writeSLIP(uint8_t type, uint8_t* data, uint32_t len) {
  uint32_t sent = 0;

  sent += write(SLIP_END);
  sent += _writeSLIP(type);

  for (uint32_t i = 0; i < len; i++) {
    sent += _writeSLIP(data[i]);
  }
  sent += write(SLIP_END);

  return sent;
}

void UartEx::setInputCallback(UartExInputCallback callback) {
  _input_callback = callback;
}

void UartEx::_slip_input_byte(uint8_t data) {
  switch (_slip_state) {
    case SLIP_STATE_RUBBISH:
      if (data == SLIP_END) {
        _rx_data_len = 0;
        _slip_state  = SLIP_STATE_OK;
      }
      break;

    case SLIP_STATE_OK:
      if (data == SLIP_END) {
        if (_rx_data_len > 0) {
          /* We have received data */
          if (_input_callback) {
            _input_callback(_rx_buf[0], &_rx_buf[1], _rx_data_len - 1);
          }
          _rx_data_len = 0;
          _slip_state  = SLIP_STATE_RUBBISH;
        }
      } else if (data == SLIP_ESC) {
        _slip_state = SLIP_STATE_ESC;
      } else {
        if (_rx_data_len < UARTEX_RX_BUF_SIZE) {
          _rx_buf[_rx_data_len++] = data;
        } else {
          /* Frame is too big to handle */
          _rx_data_len = 0;
          _slip_state  = SLIP_STATE_RUBBISH;
        }
      }
      break;

    case SLIP_STATE_ESC:
      if (data == SLIP_ESC_END) {
        if (_rx_data_len < UARTEX_RX_BUF_SIZE) {
          _rx_buf[_rx_data_len++] = data;
        } else {
          /* Frame is too big to handle */
          _rx_data_len = 0;
          _slip_state  = SLIP_STATE_RUBBISH;
        }
      } else if (data == SLIP_ESC_ESC) {
        if (_rx_data_len < UARTEX_RX_BUF_SIZE) {
          _rx_buf[_rx_data_len++] = data;
        } else {
          /* Frame is too big to handle */
          _rx_data_len = 0;
          _slip_state  = SLIP_STATE_RUBBISH;
        }
      } else {
        /* erroneous character. consider as rubbish */
        _rx_data_len = 0;
        _slip_state  = SLIP_STATE_RUBBISH;
      }
      break;

    default:
      break;
  }
}

void UartEx::poll() {
  uint32_t to_read = (uint32_t)available();

  if (_slip_enabled) {
    while (to_read > 0) {
      _slip_input_byte((uint8_t)read());
      to_read--;
    }
  } else {
    _rx_data_len = 0;

    while (to_read > 0 && _rx_data_len < UARTEX_RX_BUF_SIZE) {
      _rx_buf[_rx_data_len++] = (uint8_t)read();
      to_read--;
    }

    if (_rx_data_len > 0 && _input_callback) {
      _input_callback(SLIP_FRAME_TYPE_DBG, _rx_buf, _rx_data_len);
    }
  }
}
